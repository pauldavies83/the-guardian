package uk.co.pauldavies83.theguardian.articles

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.matcher.BundleMatchers.hasEntry
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.intent.matcher.IntentMatchers.hasExtras
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.runner.AndroidJUnit4
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest
import org.hamcrest.CoreMatchers.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import uk.co.pauldavies83.theguardian.MockWebServerTestCase
import uk.co.pauldavies83.theguardian.R

@RunWith(AndroidJUnit4::class)
class ArticlesActivityTest : MockWebServerTestCase() {

    @JvmField
    @Rule
    val testRule = IntentsTestRule(ArticlesActivity::class.java, true, false)

    @Test
    @Throws(Exception::class)
    fun listOfResultsShouldBeDisplayedWhenRetrieved() {
        testRule.launchActivity(null)

        onView(withId(R.id.error_textview))
                .check(matches(not(isDisplayed())))

        onView(withId(R.id.articles_recyclerview))
                .check(matches(isDisplayed()))
    }

    @Test
    @Throws(Exception::class)
    fun errorIsShownWhenArticlesNotRetrieved() {
        server.setDispatcher(object : Dispatcher() {
            @Throws(InterruptedException::class)
            override fun dispatch(request: RecordedRequest): MockResponse {
                return MockResponse().setResponseCode(404)
            }
        })

        testRule.launchActivity(null)

        onView(withId(R.id.articles_recyclerview))
                .check(matches(not(isDisplayed())))

        onView(withId(R.id.error_textview))
                .check(matches(isDisplayed()))
    }

    @Test
    @Throws(Exception::class)
    fun clickingAnArticleShouldLaunchTheDetailsOfThatArticle() {
        testRule.launchActivity(null)

        onView(withId(R.id.articles_recyclerview))
                .check(matches(isDisplayed()))

        onView(withText(article.title))
                .check(matches(isDisplayed()))
                .perform(click())

        intended(allOf(
                hasComponent(ArticleDetailActivity::class.java.name),
                hasExtras(hasEntry(equalTo(ARTICLE), equalTo(article)))
        ))
    }

}