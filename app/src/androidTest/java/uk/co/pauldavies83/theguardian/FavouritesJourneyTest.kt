package uk.co.pauldavies83.theguardian

import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.BoundedMatcher
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import android.view.View
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import uk.co.pauldavies83.theguardian.articles.ArticlesActivity

@RunWith(AndroidJUnit4::class)
class FavouritesJourneyTest : MockWebServerTestCase() {

    @JvmField
    @Rule
    val testRule = ActivityTestRule(ArticlesActivity::class.java, true, false)

    @Test
    fun favouritedItemRemainsFavouritedWhenReturningToArticleListAndBackToArticleDetails() {
        launchArticles_selectFirstArticle_addItToFavourites_returnToArticles()

        onView(withText(article.title))
                .check(matches(isDisplayed()))
                .perform(ViewActions.click())

        onView(withId(R.id.favourite_add_menuitem))
                .check(matches(allOf(isDisplayed(), withContentDescription(R.string.remove_from_favourites))))
                .perform(click())
                .check(matches(allOf(isDisplayed(), withContentDescription(R.string.add_to_favourites))))
    }

    @Test
    fun favouritedItemIsShownInTheFavouritesGroupInTheArticlesList() {
        launchArticles_selectFirstArticle_addItToFavourites_returnToArticles()

        checkFavouriteArticleIsShownInFavouritesGroupAndNotInArticleList()
    }

    @Test
    fun unfavouritedItemIsRemovedFromTheFavouriteGroupInTheArticlesList() {
        launchArticles_selectFirstArticle_addItToFavourites_returnToArticles()

        checkFavouriteArticleIsShownInFavouritesGroupAndNotInArticleList()

        onView(withText(article.title))
                .check(matches(isDisplayed()))
                .perform(ViewActions.click())

        onView(withId(R.id.favourite_add_menuitem))
                .check(matches(allOf(isDisplayed(), withContentDescription(R.string.remove_from_favourites))))
                .perform(click())
                .check(matches(allOf(isDisplayed(), withContentDescription(R.string.add_to_favourites))))

        Espresso.pressBack()

        onView(withId(R.id.articles_recyclerview))
                .check(matches(atPosition(0, withText(R.string.divider_label_older))))

        onView(withId(R.id.articles_recyclerview))
                .check(matches(atPosition(1, withContentDescription(article.title))))
    }


    private fun checkFavouriteArticleIsShownInFavouritesGroupAndNotInArticleList() {
        onView(withId(R.id.articles_recyclerview))
                .check(matches(atPosition(0, withText(R.string.divider_label_favourites))))

        onView(withId(R.id.articles_recyclerview))
                .check(matches(atPosition(1, withContentDescription(article.title))))

        onView(withId(R.id.articles_recyclerview))
                .check(matches(atPosition(2, withText(R.string.divider_label_older))))

        onView(withId(R.id.articles_recyclerview))
                .check(matches(not(atPosition(3, withContentDescription(article.title)))))
    }

    private fun launchArticles_selectFirstArticle_addItToFavourites_returnToArticles() {
        testRule.launchActivity(null)

        onView(withId(R.id.articles_recyclerview))
                .check(matches(isDisplayed()))

        onView(withId(R.id.articles_recyclerview))
                .check(matches(atPosition(0, withText(R.string.divider_label_older))))

        onView(withId(R.id.articles_recyclerview))
                .check(matches(atPosition(1, withContentDescription(article.title))))

        onView(withText(article.title))
                .check(matches(isDisplayed()))
                .perform(ViewActions.click())

        onView(withId(R.id.favourite_add_menuitem))
                .check(matches(allOf(isDisplayed(), withContentDescription(R.string.add_to_favourites))))
                .perform(click())
                .check(matches(allOf(isDisplayed(), withContentDescription(R.string.remove_from_favourites))))

        Espresso.pressBack()
    }

    fun atPosition(position: Int, itemMatcher: Matcher<View>): Matcher<View> {
        checkNotNull(itemMatcher)
        return object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {
            override fun describeTo(description: Description) {
                description.appendText("has item at position $position: ")
                itemMatcher.describeTo(description)
            }

            override fun matchesSafely(view: RecyclerView): Boolean {
                val viewHolder = view.findViewHolderForAdapterPosition(position) ?: return false
                return itemMatcher.matches(viewHolder.itemView)
            }
        }
    }

}