package uk.co.pauldavies83.theguardian.articles

import android.content.Intent
import android.support.test.espresso.Espresso
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.runner.AndroidJUnit4
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import uk.co.pauldavies83.theguardian.MockWebServerTestCase
import uk.co.pauldavies83.theguardian.R

@RunWith(AndroidJUnit4::class)
class ArticleDetailActivityTest : MockWebServerTestCase() {

    @JvmField
    @Rule
    val testRule = IntentsTestRule(ArticleDetailActivity::class.java, true, false)

    private fun launchActivityWithValidArticle() {
        val intent = Intent().putExtra(ARTICLE, article)
        testRule.launchActivity(intent)
    }

    @Test
    @Throws(Exception::class)
    fun articleDetailShouldBeDisplayedWhenRetrieved() {
        launchActivityWithValidArticle()

        Espresso.onView(withId(R.id.error_textview))
                .check(matches(not(isDisplayed())))

        Espresso.onView(withId(R.id.article_details_content_container))
                .check(matches(isDisplayed()))

        Espresso.onView(withId(R.id.article_details_headline_textview))
                .check(matches(allOf(isDisplayed(), withText(articleDetail.headline))))

        Espresso.onView(withId(R.id.article_details_article_textview))
                .check(matches(isDisplayed()))

        Espresso.onView(withId(R.id.favourite_add_menuitem))
                .check(matches(allOf(isDisplayed(), withContentDescription(R.string.add_to_favourites))))
    }

    @Test
    fun whenAddedToFavouritesButtonShouldUpdateToBeARemoveButton() {
        launchActivityWithValidArticle()

        Espresso.onView(withId(R.id.favourite_add_menuitem))
                .check(matches(allOf(isDisplayed(), withContentDescription(R.string.add_to_favourites))))
                .perform(click())
                .check(matches(allOf(isDisplayed(), withContentDescription(R.string.remove_from_favourites))))
    }

    @Test
    @Throws(Exception::class)
    fun errorIsShownWhenArticleIsNotRetrieved() {
        val intent = Intent().putExtra(ARTICLE, invalidArticle)
        testRule.launchActivity(intent)

        Espresso.onView(withId(R.id.article_details_content_container))
                .check(matches(not(isDisplayed())))

        Espresso.onView(withText(R.string.error_fetching_articles))
                .check(matches(isDisplayed()))
    }

}