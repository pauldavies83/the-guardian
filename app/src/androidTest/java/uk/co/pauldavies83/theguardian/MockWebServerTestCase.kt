package uk.co.pauldavies83.theguardian

import android.support.test.InstrumentationRegistry
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.After
import org.junit.Before
import uk.co.pauldavies83.theguardian.api.GuardianServiceConstants
import uk.co.pauldavies83.theguardian.testhelpers.TestArticleFactory

open class MockWebServerTestCase {

    val server: MockWebServer = MockWebServer()
    val article = TestArticleFactory().article()
    val articleDetail = TestArticleFactory().articleDetail()
    val invalidArticle = TestArticleFactory().articleWithInvalidUrl()

    @Before
    fun setUp() {
        val dispatcher = object : Dispatcher() {
            @Throws(InterruptedException::class)
            override fun dispatch(request: RecordedRequest): MockResponse {
                when {
                    request.path.contains(article.id) -> return MockResponse().setResponseCode(200).setBody(getStringFromFile("valid_detail_response.json"))
                    request.path.contains("search") -> return MockResponse().setResponseCode(200).setBody(getStringFromFile("valid_list_response.json"))
                    else -> return MockResponse().setResponseCode(404)
                }
            }
        }
        server.setDispatcher(dispatcher)
        server.start()
        GuardianServiceConstants.BASE_URL = server.url("/").toString()
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        server.shutdown()
    }


    @Throws(Exception::class)
    fun getStringFromFile(filePath: String): String {
        val stream = InstrumentationRegistry.getContext().resources.assets.open(filePath)
        val stringFromFile = stream.bufferedReader().use { it.readText() }
        stream.close()
        return stringFromFile
    }

}