package uk.co.pauldavies83.theguardian.testhelpers

import com.google.gson.Gson
import retrofit2.Response
import uk.co.pauldavies83.theguardian.api.model.ApiArticle
import uk.co.pauldavies83.theguardian.api.model.ApiArticleFieldsRoot
import uk.co.pauldavies83.theguardian.articles.SuccessResponse
import uk.co.pauldavies83.theguardian.articles.model.Article
import uk.co.pauldavies83.theguardian.articles.model.ArticleDetail
import uk.co.pauldavies83.theguardian.articles.model.ArticleMapper

class TestArticleFactory {

     fun article(): Article {
         val apiArticle = Gson().fromJson("{\n" +
                 "      \"id\": \"small-business-network/2017/jan/04/fintech-entrepreneur-arnold-schwarzeneggers-sidekick\",\n" +
                 "      \"type\": \"article\",\n" +
                 "      \"sectionId\": \"small-business-network\",\n" +
                 "      \"sectionName\": \"Guardian Small Business Network\",\n" +
                 "      \"webPublicationDate\": \"2017-01-04T07:15:17Z\",\n" +
                 "      \"webTitle\": \"The fintech entrepreneur doubling as Arnold Schwarzenegger’s sidekick\",\n" +
                 "      \"webUrl\": \"https://www.theguardian.com/small-business-network/2017/jan/04/fintech-entrepreneur-arnold-schwarzeneggers-sidekick\",\n" +
                 "      \"apiUrl\": \"small-business-network/2017/jan/04/fintech-entrepreneur-arnold-schwarzeneggers-sidekick\",\n" +
                 "      \"fields\": {\n" +
                 "        \"headline\": \"The FINTECH entrepreneur doubling as Arnold Schwarzenegger’s sidekick\",\n" +
                 "        \"thumbnail\": \"https://media.guim.co.uk/ff5dc31e4ccf8db8c86010631f9e69af5d42fbb2/0_705_3744_2246/500.jpg\"\n" +
                 "      },\n" +
                 "      \"isHosted\": false\n" +
                 "    }", ApiArticle::class.java
         )
         ArticleMapper()
         return Article(apiArticle.id,
                     apiArticle.fields.thumbnail ?: "",
                     apiArticle.sectionId,
                     apiArticle.sectionName,
                     apiArticle.webPublicationDate,
                     apiArticle.fields.headline,
                     apiArticle.apiUrl)
     }

    fun articleWithInvalidUrl(): Article {
        val apiArticle = Gson().fromJson("{\n" +
                "      \"id\": \"small-business-network/2017/jan/04/fintech-entrepreneur-arnold-schwarzeneggers-sidekick\",\n" +
                "      \"type\": \"article\",\n" +
                "      \"sectionId\": \"small-business-network\",\n" +
                "      \"sectionName\": \"Guardian Small Business Network\",\n" +
                "      \"webPublicationDate\": \"2017-01-04T07:15:17Z\",\n" +
                "      \"webTitle\": \"The fintech entrepreneur doubling as Arnold Schwarzenegger’s sidekick\",\n" +
                "      \"webUrl\": \"https://www.theguardian.com/small-business-network/2017/jan/04/fintech-entrepreneur-arnold-schwarzeneggers-sidekick\",\n" +
                "      \"apiUrl\": \"invalid\",\n" +
                "      \"fields\": {\n" +
                "        \"headline\": \"The FINTECH entrepreneur doubling as Arnold Schwarzenegger’s sidekick\",\n" +
                "        \"thumbnail\": \"https://media.guim.co.uk/ff5dc31e4ccf8db8c86010631f9e69af5d42fbb2/0_705_3744_2246/500.jpg\"\n" +
                "      },\n" +
                "      \"isHosted\": false\n" +
                "    }", ApiArticle::class.java
        )
        ArticleMapper()
        return Article(apiArticle.id,
                apiArticle.fields.thumbnail ?: "",
                apiArticle.sectionId,
                apiArticle.sectionName,
                apiArticle.webPublicationDate,
                apiArticle.fields.headline,
                apiArticle.apiUrl)
    }

    fun articleDetail(): ArticleDetail =
            (ArticleMapper().mapApiFieldsToModelDetail(Response.success(
                Gson().fromJson("{\n" +
                        "  \"response\": {\n" +
                        "    \"status\": \"ok\",\n" +
                        "    \"userTier\": \"developer\",\n" +
                        "    \"total\": 1,\n" +
                        "    \"content\": {\n" +
                        "      \"id\": \"small-business-network/2017/jan/04/fintech-entrepreneur-arnold-schwarzeneggers-sidekick\",\n" +
                        "      \"type\": \"article\",\n" +
                        "      \"sectionId\": \"small-business-network\",\n" +
                        "      \"sectionName\": \"Guardian Small Business Network\",\n" +
                        "      \"webPublicationDate\": \"2017-01-04T07:15:17Z\",\n" +
                        "      \"webTitle\": \"The fintech entrepreneur doubling as Arnold Schwarzenegger’s sidekick\",\n" +
                        "      \"webUrl\": \"https://www.theguardian.com/small-business-network/2017/jan/04/fintech-entrepreneur-arnold-schwarzeneggers-sidekick\",\n" +
                        "      \"apiUrl\": \"small-business-network/2017/jan/04/fintech-entrepreneur-arnold-schwarzeneggers-sidekick\",\n" +
                        "      \"fields\": {\n" +
                        "        \"headline\": \"The fintech entrepreneur doubling as Arnold Schwarzenegger’s sidekick\",\n" +
                        "        \"main\": \"<figure class=\\\"element element-image\\\" data-media-id=\\\"ff5dc31e4ccf8db8c86010631f9e69af5d42fbb2\\\"> <img src=\\\"https://media.guim.co.uk/ff5dc31e4ccf8db8c86010631f9e69af5d42fbb2/0_705_3744_2246/1000.jpg\\\" alt=\\\"gemma godfrey\\\" width=\\\"1000\\\" height=\\\"600\\\" class=\\\"gu-image\\\" /> <figcaption> <span class=\\\"element-image__caption\\\">Gemma Godfrey: ‘I’ve made mistakes but they’ve made me tougher’.</span> <span class=\\\"element-image__credit\\\">Photograph: Trent McMinn</span> </figcaption> </figure>\",\n" +
                        "        \"body\": \"<h2>How did your BUSINESS Moola [an online money management service] start? </h2> <p>I’m sure it’s the same for everyone in financial services: you’re often approached by friends asking you what they should do with their money [Godfrey has worked in financial management since 1997]. For me this was magnified because I was being asked the same question on television while appearing as a money and consumer expert [Godfrey’s credits include the BBC, ITV and Sky News].<br /></p> <p>There is a savings crisis in the UK. Millions [according to Deloitte <a href=\\\"https://www2.deloitte.com/content/dam/Deloitte/uk/Documents/financial-services/deloitte-uk-fs-rdr-bridging-the-advice-gap.pdf\\\">research</a> the number is 5.5 million] have a bit of money to invest but no access to financial advice.<br /></p> <p>People are making more financial transactions online and I felt this change in behaviour was not being catered for. Meanwhile, finance has been shrouded in jargon and complexity, which has alienated people.<br /></p> <p>With Moola, which was launched at the end of 2016, I wanted to offer something different. The advice is jargon-free and it provides a stripped down service to small investors. For financial advisers it takes away the regulatory and administrative burden of looking after smaller clients, making it more cost-effective.<br /></p> <aside class=\\\"element element-rich-link element--thumbnail\\\"> <p> <span>Related: </span><a href=\\\"https://www.theguardian.com/small-business-network/2016/jul/27/launched-foreign-exchange-business-no-money\\\">'I launched a foreign exchange business with no money'</a> </p> </aside>  <p>So far, we have a few thousand people signed up and we have been working with venture capital and private equity firm Octopus Investments and their network of independent financial advisers and customers for the last couple of months.<br /></p> <h2>How does Moola make money?</h2> <p>It gives users access to online investment services for a fee [the lowest amount that can be invested is £200]. We keep the price down by offering a simplified service and automating wherever possible.<br /></p> <p>The fee is a small slice of what’s invested. It covers the cost of safeguarding the money, background checks, guiding towards investment options, putting the money to work and monitoring it from then on.<br /></p> <h2>Where would you like to take it?</h2> <p>The vision is to change the landscape of finance, making it inclusive and accessible, so that everyone can grow their money, regardless of how much money they have or how much they understand about investing.</p> <p>It could help people who feel left behind financially, such as those who made their feelings known in the Brexit vote.</p> <h2>You recently appeared as an adviser to Arnold Schwarzenegger on The Celebrity Apprentice in the US – how did that opportunity come about?</h2> <p>It took seven years of blood, sweat and tears. I started from scratch in the media, dedicating time to build a network of contacts and develop the on-camera skills needed. </p> <p>Of course, I also had to build a track record in business, from firms running \$10bn [£8bn] of people’s money [Brooks Macdonald], to my own startup.<br /></p> <p>As one of few official contributors for CNBC outside of the US, I’d already been broadcasted to millions of American viewers on a regular basis. Also, the network that runs The Apprentice is NBC, the owner of CNBC, and therefore I was “in the family”.<br /></p> <p>For this new season, with Arnold Schwarzenegger as the new boss, there was even more of a focus on entertainment. And the show had moved to California from New York [and Trump Towers], so my running a financial technology startup was a perfect fit [with California’s reputation for successful technology startups].<br /></p> <p>As the boardroom adviser, my role was to assess all contestants during the tasks and then report back to Arnold on their performances and suggest who he should fire and save. In the celebrity version, the contestants battled it out for their chosen charities.<br /></p> <h2>What was it like?</h2> <p>It was fun, surreal and absolutely fascinating to see behind the scenes. It was inspiring to be around such powerful and prominent personalities. With each task kept to a time limit, it meant shooting the show was full-on. It was also hard work offering real, tangible business insights while being entertaining. But I loved every second of it.</p> <h2>What was it like to do business in front of a camera crew?</h2> <p>The biggest difference between TV and real life is the time restrictions. It’s unlikely you’d have to launch a new brand or close a deal within the 24-hour period in which an Apprentice task is shot. But the skills you need to succeed are the same, and so are the challenges. It’s just like learning about business on fast forward. <br /></p> <h2>While you were over there, did you get a sense of the buzz around the election? What were people’s views of Donald Trump?</h2> <p>Like with Brexit over here, the rise of someone outside of politics showed how split the country is in terms of the millions of people who feel disenfranchised and left behind.<br /></p> <h2>Why do you think finance is dominated by men?</h2> <p>It’s a self-perpetuating scenario where the lack of women has put off more women from rising through the ranks. But a successful business needs a balance of skills. Diversity of age, gender and background is required to represent customer needs, challenge the business strategy and deliver tailored solutions.<br /></p> <p>The way to solve the problem is to initiate a virtuous circle of [diverse] role models and mentors showing how it can be done and shifting the balance.<br /></p> <aside class=\\\"element element-rich-link element--thumbnail\\\"> <p> <span>Related: </span><a href=\\\"https://www.theguardian.com/small-business-network/2016/nov/26/classpass-cofounder-ditching-unlimited-pass-business-contacts\\\">ClassPass co-founder: ditching unlimited pass was tough</a> </p> </aside>  <h2>Is there anything you would do differently if you started again?</h2> <p>Nope. I’ve made mistakes but they made me tougher. You probably learn more from a misstep than by getting something right first time.<br /></p> <h2>What is your proudest moment?</h2> <p>Advising the legendary Arnold Schwarzenegger and being in the company of titans such as Warren Buffett, Jessica Alba, Tyra Banks and Steve Ballmer on The Celebrity Apprentice is a contender.</p> <p>Starting Moola with a talented team, backed by industry leaders and authorised by our regulator is another. They’re only beaten by marrying my soulmate, having the most amazing son and a daughter on the way.</p> <h2>What advice would you give to other aspiring entrepreneurs?</h2> <p>Solve a problem rather than sell a product, validate the need before you build, find a great team.</p> <p><strong>Sign up to become a member of the </strong><a href=\\\"https://register.theguardian.com/small-business/\\\"><strong>Guardian Small Business Network here</strong></a><strong> for more advice, insight and best practice direct to your inbox.</strong></p>\",\n" +
                        "        \"thumbnail\": \"https://media.guim.co.uk/ff5dc31e4ccf8db8c86010631f9e69af5d42fbb2/0_705_3744_2246/500.jpg\"\n" +
                        "      },\n" +
                        "      \"isHosted\": false\n" +
                        "    }\n" +
                        "  }\n" +
                        "}", ApiArticleFieldsRoot::class.java
                ))) as SuccessResponse).data
}