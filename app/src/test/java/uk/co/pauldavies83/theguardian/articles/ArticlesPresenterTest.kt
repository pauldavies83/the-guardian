package uk.co.pauldavies83.theguardian.articles

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations.initMocks
import retrofit2.Response
import uk.co.pauldavies83.theguardian.api.GuardianService
import uk.co.pauldavies83.theguardian.api.model.ApiArticle
import uk.co.pauldavies83.theguardian.api.model.ApiArticleFields
import uk.co.pauldavies83.theguardian.api.model.ApiArticleListRoot
import uk.co.pauldavies83.theguardian.api.model.ApiArticleResponse
import uk.co.pauldavies83.theguardian.articles.model.Article
import uk.co.pauldavies83.theguardian.articles.model.ArticleListEntryMapper
import uk.co.pauldavies83.theguardian.articles.model.ArticleMapper
import uk.co.pauldavies83.theguardian.persistance.InMemoryFavouritesStore
import uk.co.pauldavies83.theguardian.testhelpers.SystemTimeProviderForTest
import java.util.*

class ArticlesPresenterTest {
    @Mock private lateinit var guardianService: GuardianService
    @Mock private lateinit var view: ArticlesPresenter.View
    @Mock private lateinit var retrofitResponse: Response<ApiArticleListRoot>

    private val favouritesStore = InMemoryFavouritesStore()
    private lateinit var articlesRepository: ArticlesRepository

    private lateinit var presenter: ArticlesPresenter

    private val date = Calendar.getInstance().time

    private val article = Article("id", "thumbnail", "sectionId",
            "sectionName", date, "title", "url")
    private val articles = arrayListOf(article)

    private val apiArticle = ApiArticle("id", "sectionId", "sectionName",
            date, "title", "webUrl", "url", ApiArticleFields("", "", "", ""))
    private val apiArticles = ApiArticleListRoot(ApiArticleResponse(arrayListOf(apiArticle)))

    private val refreshObservable = Observable.just<Any>({})
    private val articleClickedObservable = Observable.just(article)

    @Before
    @Throws(Exception::class)
    fun setUp() {
        initMocks(this)

        articlesRepository = ArticlesRepository(guardianService, ArticleMapper(), favouritesStore)

        given(guardianService.searchArticles(ArgumentMatchers.any())).willReturn(Single.just(retrofitResponse))
        given(view.onRefreshAction()).willReturn(refreshObservable)
        given(view.onArticleClicked()).willReturn(articleClickedObservable)

        presenter = ArticlesPresenter(Schedulers.trampoline(), Schedulers.trampoline(),
                articlesRepository, ArticleListEntryMapper(SystemTimeProviderForTest()))
    }

    @Test
    @Throws(Exception::class)
    fun registerAndValidResponseFromRepository() {
        given(retrofitResponse.body()).willReturn(apiArticles)
        given(retrofitResponse.isSuccessful()).willReturn(true)

        presenter.register(view)

        verify(guardianService).searchArticles(ArgumentMatchers.anyString())
        verify(view).onRefreshAction()
        verify(view).onArticleClicked()

        assert(refreshObservable.test().equals(articles))
        assert(articleClickedObservable.test().equals(article))

        verify(view).showRefreshing(true)
        verify(view).showArticles(ArgumentMatchers.anyList())
        verify(view).showRefreshing(false)
    }

    @Test
    @Throws(Exception::class)
    fun registerButExceptionFromRepository() {
        given(retrofitResponse.isSuccessful()).willReturn(false)

        presenter.register(view)

        verify(guardianService).searchArticles(ArgumentMatchers.anyString())
        verify(view).onRefreshAction()
        verify(view).onArticleClicked()

        assert(refreshObservable.test().equals(articles))
        assert(articleClickedObservable.test().equals(article))

        verify(view).showRefreshing(true)

        verify(view).showError()
        verify(view).showRefreshing(false)
    }

}