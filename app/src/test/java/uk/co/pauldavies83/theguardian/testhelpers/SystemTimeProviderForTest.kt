package uk.co.pauldavies83.theguardian.testhelpers

import uk.co.pauldavies83.theguardian.common.SystemTimeProvider
import java.util.*

class SystemTimeProviderForTest : SystemTimeProvider() {

    override fun getInstance(): Calendar {
        val cal = Calendar.getInstance(Locale.UK)
        cal.set(Calendar.YEAR, 2017)
        cal.set(Calendar.MONTH, 10)
        cal.set(Calendar.DAY_OF_MONTH, 23)
        return cal
    }

}