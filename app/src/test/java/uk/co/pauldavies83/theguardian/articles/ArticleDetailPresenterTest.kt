package uk.co.pauldavies83.theguardian.articles

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.ArgumentMatchers.matches
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.Spy
import retrofit2.Response
import uk.co.pauldavies83.theguardian.api.GuardianService
import uk.co.pauldavies83.theguardian.api.model.ApiArticleFields
import uk.co.pauldavies83.theguardian.api.model.ApiArticleFieldsContent
import uk.co.pauldavies83.theguardian.api.model.ApiArticleFieldsResponse
import uk.co.pauldavies83.theguardian.api.model.ApiArticleFieldsRoot
import uk.co.pauldavies83.theguardian.articles.model.Article
import uk.co.pauldavies83.theguardian.articles.model.ArticleDetail
import uk.co.pauldavies83.theguardian.articles.model.ArticleMapper
import uk.co.pauldavies83.theguardian.persistance.InMemoryFavouritesStore
import java.util.*

class ArticleDetailPresenterTest {
    @Mock private lateinit var guardianService: GuardianService
    @Mock private lateinit var view: ArticleDetailPresenter.View
    @Mock private lateinit var retrofitResponse: Response<ApiArticleFieldsRoot>
    @Spy private val favouritesStore = InMemoryFavouritesStore()

    private lateinit var articlesRepository: ArticlesRepository
    private lateinit var presenter: ArticleDetailPresenter

    private val articleUrl: String = "ARTICLE-URL"
    private val article = Article("id", "thumbnail","sectionId", "sectionName",
            Calendar.getInstance().time, "title", articleUrl)
    private val articleDetail = ArticleDetail("headline", "main", "body", "thumbnail")
    private val apiArticleFieldsRoot =
            ApiArticleFieldsRoot(
            ApiArticleFieldsResponse(
            ApiArticleFieldsContent(
            ApiArticleFields("headline", "main", "body", "thumbnail"))))

    private val favouriteButtonClickObservable = Observable.just(article)

    @Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        articlesRepository = ArticlesRepository(guardianService, ArticleMapper(), favouritesStore)

        given(retrofitResponse.isSuccessful()).willReturn(true)
        given(retrofitResponse.body()).willReturn(apiArticleFieldsRoot)
        given(guardianService.getArticle(anyString(), anyString())).willReturn(Single.just(retrofitResponse))
        given(view.onArticleFavouriteToggled()).willReturn(favouriteButtonClickObservable)

        presenter = ArticleDetailPresenter(Schedulers.trampoline(), Schedulers.trampoline(),
                articlesRepository, article)
        presenter.register(view)
    }

    @Test
    @Throws(Exception::class)
    fun registerAndArticleAlreadyFavourited() {
        favouritesStore.put(article)

        verify(guardianService).getArticle(matches(article.url), anyString())
        verify(favouritesStore).contains(article)
        verify(view).showArticleDetail(articleDetail)
        verify(view).setArticleIsFavourite(true)
    }

    @Test
    @Throws(Exception::class)
    fun registerAndArticleNotAlreadyFavourited() {
        verify(guardianService).getArticle(matches(article.url), anyString())
        verify(favouritesStore).contains(article)
        verify(view).showArticleDetail(articleDetail)
        verify(view).setArticleIsFavourite(false)
    }

    @Test
    fun whenAddFavouriteClickedArticleIsAddedToStore() {
        favouriteButtonClickObservable.test()
        assert(favouritesStore.toggle(article).test().equals(true))
        verify(view).setArticleIsFavourite(true)
        assert(favouritesStore.contains(article).blockingGet())
    }

    @Test
    fun whenAddFavouriteClickedTwiceArticleIsAddedToStoreAndRemovedFromStore() {
        favouriteButtonClickObservable.test()
        assert(favouritesStore.toggle(article).test().equals(true))
        verify(view).setArticleIsFavourite(true)
        assert(favouritesStore.contains(article).blockingGet())

        favouriteButtonClickObservable.test()
        assert(favouritesStore.toggle(article).test().equals(false))
        verify(view).setArticleIsFavourite(false)
        assert(!favouritesStore.contains(article).blockingGet())
    }
}