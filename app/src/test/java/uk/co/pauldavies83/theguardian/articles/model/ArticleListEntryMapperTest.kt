package uk.co.pauldavies83.theguardian.articles.model

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import uk.co.pauldavies83.theguardian.testhelpers.SystemTimeProviderForTest
import java.util.Calendar.*

class ArticleListEntryMapperTest {

    private val timeProvider = SystemTimeProviderForTest()

    lateinit var todayArticle : Article
    lateinit var mondayArticle : Article
    lateinit var sundayArticle : Article
    lateinit var threeWeeksAgoArticle : Article
    lateinit var aWeekLastMondayArticle: Article
    lateinit var aWeekLastSundayArticle: Article

    fun getThisWeeksArticleList() : ArrayList<Article> {
        todayArticle = Article("today", "thumbnail", "sectionId",
                "sectionName", timeProvider.getInstance().time, "title", "url")

        val monday = timeProvider.getInstance()
        monday.set(DAY_OF_WEEK, MONDAY)

        mondayArticle = Article("monday", "thumbnail", "sectionId",
                "sectionName", monday.time, "title", "url")

        return arrayListOf(todayArticle, mondayArticle)
    }

    private fun getLastWeeksArticleList(): ArrayList<Article> {
        val sunday = timeProvider.getInstance()
        sunday.set(DAY_OF_WEEK, MONDAY)
        sunday.set(DAY_OF_YEAR, sunday.get(DAY_OF_YEAR) - 1)

        sundayArticle = Article("sunday", "thumbnail", "sectionId",
                "sectionName", sunday.time, "title", "url")

        val aWeekLastMonday = timeProvider.getInstance()
        aWeekLastMonday.set(DAY_OF_WEEK, MONDAY)
        aWeekLastMonday.set(DAY_OF_YEAR, aWeekLastMonday.get(DAY_OF_YEAR) - 7)

        aWeekLastMondayArticle = Article("aWeekLastMonday", "thumbnail", "sectionId",
                "sectionName", aWeekLastMonday.time, "title", "url")

        return arrayListOf(sundayArticle, aWeekLastMondayArticle)
    }


    fun getThreeWeeksAgoArticleList() : ArrayList<Article> {
        val aWeekLastSunday = timeProvider.getInstance()
        aWeekLastSunday.set(DAY_OF_WEEK, MONDAY)
        aWeekLastSunday.set(DAY_OF_YEAR, aWeekLastSunday.get(DAY_OF_YEAR) - 8)

        aWeekLastSundayArticle = Article("aWeekLastSunday", "thumbnail", "sectionId",
                "sectionName", aWeekLastSunday.time, "title", "url")

        val threeWeeksAgo = timeProvider.getInstance()
        threeWeeksAgo.set(DAY_OF_WEEK, MONDAY)
        threeWeeksAgo.set(DAY_OF_YEAR, threeWeeksAgo.get(DAY_OF_YEAR) - 9)

        threeWeeksAgoArticle = Article("threeWeeksAgo", "thumbnail", "sectionId",
                "sectionName", threeWeeksAgo.time, "title", "url")

        return arrayListOf(aWeekLastSundayArticle, threeWeeksAgoArticle)
    }

    @Test
    fun whenOnlyHaveThisWeeksArticles() {
        val listOfArticleListEntrys = ArticleListEntryMapper(timeProvider).insertDividerItemsIntoArticleList(getThisWeeksArticleList(), emptyList())

        val expectedArticleListEntrys = listOf<ArticleListEntry>(
                ThisWeekDivider(), ArticleListEntryItem(todayArticle), ArticleListEntryItem(mondayArticle)
        )

        assertThat(listOfArticleListEntrys, equalTo(expectedArticleListEntrys))
    }

    @Test
    fun whenOnlyHaveLastWeeksArticles() {
        val listOfArticleListEntrys = ArticleListEntryMapper(timeProvider).insertDividerItemsIntoArticleList(getLastWeeksArticleList(), emptyList())

        val expectedArticleListEntrys = listOf<ArticleListEntry>(
                LastWeekDivider(), ArticleListEntryItem(sundayArticle), ArticleListEntryItem(aWeekLastMondayArticle)
        )

        assertThat(listOfArticleListEntrys, equalTo(expectedArticleListEntrys))
    }

    @Test
    fun whenOnlyHaveOlderThanLastWeeksArticles() {
        val listOfArticleListEntrys = ArticleListEntryMapper(timeProvider).insertDividerItemsIntoArticleList(getThreeWeeksAgoArticleList(), emptyList())

        val expectedArticleListEntrys = listOf<ArticleListEntry>(
                OlderDivider(), ArticleListEntryItem(aWeekLastSundayArticle), ArticleListEntryItem(threeWeeksAgoArticle)
        )

        assertThat(listOfArticleListEntrys, equalTo(expectedArticleListEntrys))
    }

    @Test
    fun whenHaveThisWeeksAndOlderThanLastWeeksArticles() {
        val articles = getThisWeeksArticleList()
        articles.addAll(getThreeWeeksAgoArticleList())

        val listOfArticleListEntrys = ArticleListEntryMapper(timeProvider).insertDividerItemsIntoArticleList(articles, emptyList())

        val expectedArticleListEntrys = listOf<ArticleListEntry>(
                ThisWeekDivider(), ArticleListEntryItem(todayArticle), ArticleListEntryItem(mondayArticle),
                OlderDivider(), ArticleListEntryItem(aWeekLastSundayArticle), ArticleListEntryItem(threeWeeksAgoArticle)
        )

        assertThat(listOfArticleListEntrys, equalTo(expectedArticleListEntrys))
    }

    @Test
    fun whenHaveLastWeeksAndOlderThanLastWeeksArticles() {
        val articles = getLastWeeksArticleList()
        articles.addAll(getThreeWeeksAgoArticleList())

        val listOfArticleListEntrys = ArticleListEntryMapper(timeProvider).insertDividerItemsIntoArticleList(articles, emptyList())

        val expectedArticleListEntrys = listOf<ArticleListEntry>(
                LastWeekDivider(), ArticleListEntryItem(sundayArticle), ArticleListEntryItem(aWeekLastMondayArticle),
                OlderDivider(), ArticleListEntryItem(aWeekLastSundayArticle), ArticleListEntryItem(threeWeeksAgoArticle)
        )

        assertThat(listOfArticleListEntrys, equalTo(expectedArticleListEntrys))
    }

    @Test
    fun whenHaveThisWeeksAndLastWeeksArticles() {
        val articles = getThisWeeksArticleList()
        articles.addAll(getLastWeeksArticleList())

        val listOfArticleListEntrys = ArticleListEntryMapper(timeProvider).insertDividerItemsIntoArticleList(articles, emptyList())

        val expectedArticleListEntrys = listOf<ArticleListEntry>(
                ThisWeekDivider(), ArticleListEntryItem(todayArticle), ArticleListEntryItem(mondayArticle),
                LastWeekDivider(), ArticleListEntryItem(sundayArticle), ArticleListEntryItem(aWeekLastMondayArticle)
        )

        assertThat(listOfArticleListEntrys, equalTo(expectedArticleListEntrys))
    }

    @Test
    fun whenHaveThisWeeksAndLastWeeksAndOlderThanLastWeeksArticles() {
        val articles = getThisWeeksArticleList()
        articles.addAll(getLastWeeksArticleList())
        articles.addAll(getThreeWeeksAgoArticleList())

        val listOfArticleListEntrys = ArticleListEntryMapper(timeProvider).insertDividerItemsIntoArticleList(articles, emptyList())

        val expectedArticleListEntrys = listOf<ArticleListEntry>(
                ThisWeekDivider(), ArticleListEntryItem(todayArticle), ArticleListEntryItem(mondayArticle),
                LastWeekDivider(), ArticleListEntryItem(sundayArticle), ArticleListEntryItem(aWeekLastMondayArticle),
                OlderDivider(), ArticleListEntryItem(aWeekLastSundayArticle), ArticleListEntryItem(threeWeeksAgoArticle)
        )

        assertThat(listOfArticleListEntrys, equalTo(expectedArticleListEntrys))
    }

}

