package com.monzo.androidtest.persistance

import com.monzo.androidtest.articles.model.Article
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import java.util.*

class InMemoryFavouritesStoreTest {

    val articleOne = Article("1", "thumbnail","sectionId", "sectionName",
            Calendar.getInstance().time, "title", "url")
    val articleTwo = Article("2", "thumbnail","sectionId", "sectionName",
            Calendar.getInstance().time, "title", "url")
    val articleThree = Article("3", "thumbnail","sectionId", "sectionName",
            Calendar.getInstance().time, "title", "url")

    @Test
    @Throws(Exception::class)
    fun favouritesAreRerturnedInOrderAdded() {
        val store = InMemoryFavouritesStore()

        store.put(articleOne)
        store.put(articleTwo)
        store.put(articleThree)

        assertThat(store.favourites().blockingGet(), equalTo(listOf<Article>(articleOne, articleTwo, articleThree)))
    }

    @Test
    @Throws(Exception::class)
    fun favouritesAreStillRerturnedInOrderAddedIfItemsAreRemovedAndReAdded() {
        val store = InMemoryFavouritesStore()

        store.put(articleOne)
        store.put(articleTwo)
        store.toggle(articleOne)
        store.put(articleThree)
        store.toggle(articleOne)

        assertThat(store.favourites().blockingGet(), equalTo(listOf<Article>(articleTwo, articleThree, articleOne)))
    }
}
