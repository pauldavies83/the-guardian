package uk.co.pauldavies83.theguardian.api.model

data class ApiArticleResponse(val results: List<ApiArticle>)
