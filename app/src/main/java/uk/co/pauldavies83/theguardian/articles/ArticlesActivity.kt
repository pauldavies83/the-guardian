package uk.co.pauldavies83.theguardian.articles

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_article_list.*
import uk.co.pauldavies83.theguardian.HeadlinesApp
import uk.co.pauldavies83.theguardian.R
import uk.co.pauldavies83.theguardian.api.GuardianServiceConstants
import uk.co.pauldavies83.theguardian.articles.model.Article
import uk.co.pauldavies83.theguardian.articles.model.ArticleListEntry
import uk.co.pauldavies83.theguardian.common.Event

class ArticlesActivity : AppCompatActivity(), ArticlesPresenter.View {

    private lateinit var presenter: ArticlesPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_article_list)
        setSupportActionBar(toolbar)
        toolbar.setOnClickListener { articles_recyclerview.smoothScrollToPosition(0) }

        articles_recyclerview.layoutManager = LinearLayoutManager(this)
        articles_recyclerview.adapter = ArticleAdapter()
    }

    override fun onResume() {
        super.onResume()
        presenter = HeadlinesApp.from(applicationContext).injectIntoArticlesPresenter(
                GuardianServiceConstants.BASE_URL,
                this.resources.getString(R.string.guardian_api_key),
                cacheDir
        )
        presenter.register(this)
    }

    override fun onPause() {
        presenter.unregister()
        super.onPause()
    }

    override fun showArticles(articles: List<ArticleListEntry>) {
        (articles_recyclerview.adapter as ArticleAdapter).showArticles(articles)
        articles_recyclerview.visibility = View.VISIBLE
        error_textview.visibility = View.GONE
    }

    override fun showError() {
        error_textview.visibility = View.VISIBLE
    }

    override fun onArticleClicked(): Observable<Article> {
        return (articles_recyclerview.adapter as ArticleAdapter).recyclerViewItemSelectedObservable
    }

    override fun onRefreshAction(): Observable<Any> {
        return Observable.create<Any> { emitter ->
            articles_swiperefreshlayout.setOnRefreshListener { emitter.onNext(Event.IGNORE) }
            emitter.setCancellable { articles_swiperefreshlayout.setOnRefreshListener(null) }
        }.startWith(Event.IGNORE)
    }

    override fun openArticleDetail(article: Article) {
        val intent = Intent(this, ArticleDetailActivity::class.java)
        intent.putExtra(ARTICLE, article)
        startActivity(intent)
    }

    override fun showRefreshing(isRefreshing: Boolean) {
        articles_swiperefreshlayout.isRefreshing = isRefreshing
    }
}
