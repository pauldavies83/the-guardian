package uk.co.pauldavies83.theguardian.articles

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.Menu
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_article_detail.*
import uk.co.pauldavies83.theguardian.HeadlinesApp
import uk.co.pauldavies83.theguardian.R
import uk.co.pauldavies83.theguardian.api.GuardianServiceConstants
import uk.co.pauldavies83.theguardian.articles.model.Article
import uk.co.pauldavies83.theguardian.articles.model.ArticleDetail

const val ARTICLE: String = "article_key"

class ArticleDetailActivity : AppCompatActivity() , ArticleDetailPresenter.View  {

    private val favouriteSubject = PublishSubject.create<Article>()
    private var isFavourite: Boolean = false
    private lateinit var article: Article
    lateinit var presenter : ArticleDetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article_detail)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        article = intent?.getParcelableExtra<Article>(ARTICLE) as Article

        presenter = HeadlinesApp.from(applicationContext).injectIntoArticleDetailPresenter(
                GuardianServiceConstants.BASE_URL,
                this.resources.getString(R.string.guardian_api_key),
                article,
                cacheDir)
        presenter.register(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_article_details, menu)
        menu?.findItem(R.id.favourite_add_menuitem)?.setVisible(false)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val favouriteMenuItem = menu?.findItem(R.id.favourite_add_menuitem)
        when (isFavourite) {
            true -> {
                favouriteMenuItem?.setIcon(R.drawable.ic_favorite_24dp)
                favouriteMenuItem?.setTitle(R.string.remove_from_favourites)
            }
            false -> {
                favouriteMenuItem?.setIcon(R.drawable.ic_favorite_border_24dp)
                favouriteMenuItem?.setTitle(R.string.add_to_favourites)
            }
        }
        favouriteMenuItem?.setVisible(true)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.favourite_add_menuitem) {
            favouriteSubject.onNext(article)
            return true
        }
        return false
    }

    override fun onArticleFavouriteToggled() = favouriteSubject

    override fun setArticleIsFavourite(isFavourite: Boolean) {
        this.isFavourite = isFavourite
        invalidateOptionsMenu()
    }

    override fun showArticleDetail(articleDetail: ArticleDetail) {
        progress_bar.visibility = GONE
        article_details_content_container.visibility = VISIBLE

        Glide.with(this)
                .load(articleDetail.thumbnail)
                .apply(RequestOptions.centerInsideTransform())
                .into(article_illustration_imageview)

        article_details_headline_textview.visibility = VISIBLE
        article_details_headline_textview.text = articleDetail.headline

        article_details_article_textview.visibility = VISIBLE
        article_details_article_textview.movementMethod = LinkMovementMethod.getInstance()
        article_details_article_textview.text = Html.fromHtml(articleDetail.body).toString()
    }

    override fun showError() {
        article_details_content_container.visibility = GONE
        progress_bar.visibility = GONE
        error_textview.visibility = VISIBLE
    }

}
