package uk.co.pauldavies83.theguardian.api.model

data class ApiArticleFieldsResponse(val content: ApiArticleFieldsContent)