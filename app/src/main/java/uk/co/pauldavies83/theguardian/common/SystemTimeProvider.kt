package uk.co.pauldavies83.theguardian.common

import java.util.*

open class SystemTimeProvider {
    open fun getInstance(): Calendar {
        return Calendar.getInstance(Locale.UK)
    }

    open fun fromThisWeek(date: Date) : Boolean {
        return date.after(getStartOfThisWeek())
    }

    open fun fromLastWeek(date: Date) : Boolean {
        return date.before(getStartOfThisWeek())
                && date.after(getStartOfLastWeek())
    }

    private fun getStartOfThisWeek() : Date {
        val cal = getInstance()
        cal.set(Calendar.DAY_OF_WEEK, cal.firstDayOfWeek)
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        return cal.time
    }

    private fun getStartOfLastWeek() : Date {
        val cal = getInstance()
        cal.set(Calendar.DAY_OF_WEEK, cal.firstDayOfWeek)
        cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR) - 7)
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        return cal.time
    }

}