package uk.co.pauldavies83.theguardian.api.model

data class ApiArticleFieldsContent(val fields: ApiArticleFields)