package uk.co.pauldavies83.theguardian.persistance

import io.reactivex.Single
import uk.co.pauldavies83.theguardian.articles.model.Article

class InMemoryFavouritesStore : FavouriteStorageProtocol {

    private val favourites = linkedSetOf<Article>()

    override fun toggle(article: Article): Single<Boolean> {
        if (favourites.contains(article)) {
            favourites.remove(article)
            return Single.just(false)
        } else {
            favourites.add(article)
            return Single.just(true)
        }
    }

    override fun contains(article: Article): Single<Boolean> {
        return Single.just(favourites.contains(article))
    }

    override fun favourites(): Single<List<Article>> {
        return Single.just(favourites.toList())
    }

    fun put(article: Article) {
        favourites.add(article)
    }

}