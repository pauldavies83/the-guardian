package uk.co.pauldavies83.theguardian.articles.model

import retrofit2.Response
import uk.co.pauldavies83.theguardian.api.model.ApiArticleFieldsRoot
import uk.co.pauldavies83.theguardian.api.model.ApiArticleListRoot
import uk.co.pauldavies83.theguardian.articles.ErrorResponse
import uk.co.pauldavies83.theguardian.articles.RepositoryResponse
import uk.co.pauldavies83.theguardian.articles.SuccessResponse

class ArticleMapper {

    fun mapApiFieldsToModelDetail(networkResponse: Response<ApiArticleFieldsRoot>): RepositoryResponse<ArticleDetail> {
        if (networkResponse.isSuccessful) {
            val fields = networkResponse.body()!!.response.content.fields

            return SuccessResponse(ArticleDetail(
                fields.headline ?: "",
                fields.main,
                fields.body,
                fields.thumbnail
            ))
        } else {
            return ErrorResponse()
        }
    }

    fun mapApiListToModelList(networkResponse: Response<ApiArticleListRoot>): RepositoryResponse<List<Article>> {
        when {
            networkResponse.isSuccessful -> {
                val articles = networkResponse.body()!!.response.results.map {
                    Article(
                        it.id,
                        it.fields.thumbnail ?: "",
                        it.sectionId,
                        it.sectionName,
                        it.webPublicationDate,
                        it.fields.headline,
                        it.apiUrl
                    )
                }
                return SuccessResponse(articles)
            }
            else -> return ErrorResponse()
        }
    }

}
