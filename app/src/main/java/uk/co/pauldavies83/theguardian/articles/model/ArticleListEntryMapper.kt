package uk.co.pauldavies83.theguardian.articles.model

import uk.co.pauldavies83.theguardian.common.SystemTimeProvider

class ArticleListEntryMapper(val time: SystemTimeProvider) {

    fun insertDividerItemsIntoArticleList(articles: List<Article>, favourites: List<Article>) : List<ArticleListEntry> {
        val favouriteEntries = arrayListOf<ArticleListEntry>(FavouritesDivider())
        favourites.mapTo(favouriteEntries) { ArticleListEntryItem(it) }

        val thisWeekEntries = arrayListOf<ArticleListEntry>(ThisWeekDivider())
        val lastWeekEntries = arrayListOf<ArticleListEntry>(LastWeekDivider())
        val olderEntries = arrayListOf<ArticleListEntry>(OlderDivider())

        for (article in articles) {
            if (!favourites.contains(article)) {
                when {
                    time.fromThisWeek(article.published) -> thisWeekEntries.add(ArticleListEntryItem(article))
                    time.fromLastWeek(article.published) -> lastWeekEntries.add(ArticleListEntryItem(article))
                    else -> olderEntries.add(ArticleListEntryItem(article))
                }
            }
        }

        val articleEntries = arrayListOf<ArticleListEntry>()
        if (favouriteEntries.size > 1) articleEntries.addAll(favouriteEntries)
        if (thisWeekEntries.size > 1) articleEntries.addAll(thisWeekEntries)
        if (lastWeekEntries.size > 1) articleEntries.addAll(lastWeekEntries)
        if (olderEntries.size > 1) articleEntries.addAll(olderEntries)
        return articleEntries
    }

}