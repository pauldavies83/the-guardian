package uk.co.pauldavies83.theguardian.articles

import io.reactivex.Observable
import io.reactivex.Scheduler
import uk.co.pauldavies83.theguardian.articles.model.Article
import uk.co.pauldavies83.theguardian.articles.model.ArticleListEntry
import uk.co.pauldavies83.theguardian.articles.model.ArticleListEntryMapper
import uk.co.pauldavies83.theguardian.common.BasePresenter
import uk.co.pauldavies83.theguardian.common.BasePresenterView

internal class ArticlesPresenter(private val uiScheduler: Scheduler, private val ioScheduler: Scheduler, private val articlesRepository: ArticlesRepository, private val articleListEntryMapper: ArticleListEntryMapper) : BasePresenter<ArticlesPresenter.View>() {

    override fun register(view: View) {
        super.register(view)

        addToUnsubscribe(
            view.onRefreshAction()
            .doOnNext { it -> view.showRefreshing(true) }
            .switchMapSingle { ignored ->
                articlesRepository.latestFintechArticles()
                        .subscribeOn(ioScheduler)
                        .observeOn(uiScheduler)
            }
            .subscribe { articlesResult ->
                when {
                    articlesResult is SuccessResponse<List<Article>> ->
                        articlesRepository.favouriteArticles().subscribe( { favouritesResult ->
                            run {
                                view.showRefreshing(false)
                                when {
                                    favouritesResult is SuccessResponse<List<Article>> -> {
                                        view.showArticles(articleListEntryMapper.insertDividerItemsIntoArticleList(articlesResult.data, favouritesResult.data))
                                    }
                                    else -> view.showError()
                                }
                            }
                        })
                    else -> {
                        view.showRefreshing(false)
                        view.showError()
                    }
                }
            }
        )

        addToUnsubscribe(view.onArticleClicked()
                .subscribe(view::openArticleDetail))
    }

    internal interface View : BasePresenterView {
        fun showRefreshing(isRefreshing: Boolean)
        fun showArticles(articles: List<ArticleListEntry>)

        fun showError()

        fun onArticleClicked(): Observable<Article>
        fun onRefreshAction(): Observable<Any>

        fun openArticleDetail(article: Article)
    }
}