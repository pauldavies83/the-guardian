package uk.co.pauldavies83.theguardian.api.model

data class ApiArticleFieldsRoot(val response: ApiArticleFieldsResponse)
