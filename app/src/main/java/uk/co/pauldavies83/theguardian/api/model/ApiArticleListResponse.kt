package uk.co.pauldavies83.theguardian.api.model


data class ApiArticleListResponse(val response: ApiArticleList)
