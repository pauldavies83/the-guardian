package uk.co.pauldavies83.theguardian.api.model

data class ApiArticleFields(
        val headline: String,
        val main: String,
        val body: String,
        val thumbnail: String
)
