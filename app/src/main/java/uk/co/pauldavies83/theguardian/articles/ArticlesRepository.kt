package uk.co.pauldavies83.theguardian.articles

import io.reactivex.Single
import uk.co.pauldavies83.theguardian.api.GuardianService
import uk.co.pauldavies83.theguardian.articles.model.Article
import uk.co.pauldavies83.theguardian.articles.model.ArticleDetail
import uk.co.pauldavies83.theguardian.articles.model.ArticleMapper
import uk.co.pauldavies83.theguardian.persistance.FavouriteStorageProtocol

class ArticlesRepository(private val guardianService: GuardianService,
                         private val articleMapper: ArticleMapper,
                         private val favouritesStore: FavouriteStorageProtocol) {

    fun latestFintechArticles(): Single<RepositoryResponse<List<Article>>> {
        return guardianService.searchArticles("fintech,brexit")
                .map(articleMapper::mapApiListToModelList)
                .onErrorReturn {
                    ignored -> ErrorResponse()
                }
    }

    fun getArticle(articleUrl: String): Single<RepositoryResponse<ArticleDetail>> {
        return guardianService.getArticle(articleUrl, "main,body,headline,thumbnail")
                .map(articleMapper::mapApiFieldsToModelDetail)
    }

    fun toggleArticleFavourited(articleUrl: Article): Single<RepositoryResponse<Boolean>> {
        return favouritesStore.toggle(articleUrl).map { SuccessResponse(it) }
    }

    fun isArticleFavourite(articleUrl: Article): Single<RepositoryResponse<Boolean>> {
        return favouritesStore.contains(articleUrl).map { SuccessResponse(it) }
    }

    fun favouriteArticles(): Single<RepositoryResponse<List<Article>>> {
        return favouritesStore.favourites().map { SuccessResponse(it) }
    }

}

sealed class RepositoryResponse<T>

data class SuccessResponse<T>(val data : T) : RepositoryResponse<T>()
class ErrorResponse<T>() : RepositoryResponse<T>()