package uk.co.pauldavies83.theguardian.api.model

data class ApiArticleList(val results: List<ApiArticle>)
