package uk.co.pauldavies83.theguardian.articles.model

data class ArticleDetail(
    val headline: String,
    val main: String,
    val body: String,
    val thumbnail: String
)