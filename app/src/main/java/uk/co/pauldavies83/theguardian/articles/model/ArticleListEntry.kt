package uk.co.pauldavies83.theguardian.articles.model

sealed class ArticleListEntry

data class ArticleListEntryItem(val article: Article): ArticleListEntry()

sealed class ArticleListEntryDivider() : ArticleListEntry()

class FavouritesDivider() : ArticleListEntryDivider() {
    override fun equals(other: Any?): Boolean {
        return other is FavouritesDivider
    }
}

class ThisWeekDivider() : ArticleListEntryDivider() {
    override fun equals(other: Any?): Boolean {
        return other is ThisWeekDivider
    }
}

class LastWeekDivider() : ArticleListEntryDivider() {
    override fun equals(other: Any?): Boolean {
        return other is LastWeekDivider
    }
}

class OlderDivider() : ArticleListEntryDivider() {
    override fun equals(other: Any?): Boolean {
        return other is OlderDivider
    }
}