package uk.co.pauldavies83.theguardian.api.model

data class ApiArticleListRoot(val response: ApiArticleResponse)
