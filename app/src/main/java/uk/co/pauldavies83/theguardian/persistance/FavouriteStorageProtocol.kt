package uk.co.pauldavies83.theguardian.persistance

import io.reactivex.Single
import uk.co.pauldavies83.theguardian.articles.model.Article

interface FavouriteStorageProtocol {
    fun toggle(article: Article): Single<Boolean>
    fun contains(article: Article): Single<Boolean>
    fun favourites(): Single<List<Article>>
}