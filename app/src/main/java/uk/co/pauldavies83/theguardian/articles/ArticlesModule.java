package uk.co.pauldavies83.theguardian.articles;

import com.google.gson.Gson;

import java.io.File;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Cache;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.pauldavies83.theguardian.api.GuardianService;
import uk.co.pauldavies83.theguardian.articles.model.Article;
import uk.co.pauldavies83.theguardian.articles.model.ArticleListEntryMapper;
import uk.co.pauldavies83.theguardian.articles.model.ArticleMapper;
import uk.co.pauldavies83.theguardian.common.SystemTimeProvider;
import uk.co.pauldavies83.theguardian.persistance.InMemoryFavouritesStore;

public class ArticlesModule {
    private static final String HEADER_API_KEY = "api-key";
    private static final int CACHE_SIZE = 10 * 1024 * 1024; // 10MB

    private ArticlesRepository articlesRepository;
    private ArticleListEntryMapper articleListEntryMapper;

    ArticlesPresenter injectIntoArticlesPresenter(@NonNull String guardianBaseUrl, @NonNull String guardianApiKey, File cacheDir) {
        if (articlesRepository == null) {
            createArticlesRepository(guardianBaseUrl, guardianApiKey, cacheDir);
        }
        if (articleListEntryMapper == null) {
            articleListEntryMapper = new ArticleListEntryMapper(new SystemTimeProvider());
        }
        return new ArticlesPresenter(AndroidSchedulers.mainThread(), Schedulers.io(), this.articlesRepository, articleListEntryMapper);
    }

    ArticleDetailPresenter injectIntoArticleDetailPresenter(@NonNull String guardianBaseUrl, @NonNull String guardianApiKey, @NonNull Article article, File cacheDir) {
        if (articlesRepository == null) {
            createArticlesRepository(guardianBaseUrl, guardianApiKey, cacheDir);
        }
        return new ArticleDetailPresenter(AndroidSchedulers.mainThread(), Schedulers.io(), articlesRepository, article);
    }

    private void createArticlesRepository(@NonNull String guardianBaseUrl, @NonNull String guardianApiKey, File cacheDir) {
        articlesRepository = new ArticlesRepository(createGuardianService(guardianBaseUrl, guardianApiKey, cacheDir), new ArticleMapper(), new InMemoryFavouritesStore());
    }

    private GuardianService createGuardianService(String guardianBaseUrl, String guardianApiKey, File cacheDir) {
        return new Retrofit.Builder()
                .baseUrl(guardianBaseUrl)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .client(createOkHttpClient(guardianApiKey, cacheDir))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(GuardianService.class);
    }

    private OkHttpClient createOkHttpClient(String guardianApiKey, File cacheDir) {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .cache(new Cache(cacheDir, CACHE_SIZE))
                .addNetworkInterceptor(getCacheControlInterceptor())
                .addInterceptor(getAuthInterceptor(guardianApiKey))
                .addInterceptor(loggingInterceptor)
                .connectTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                .build();
    }

    private Interceptor getAuthInterceptor(final String guardianApiKey) {
        return chain -> {
            Request original = chain.request();
            Headers.Builder hb = original.headers().newBuilder();
            hb.add(HEADER_API_KEY, guardianApiKey);
            return chain.proceed(original.newBuilder().headers(hb.build()).build());
        };
    }

    /* override caching header on responses to cache for 1 minute. in reality, would rely on the
       http server to implement this header */
    private Interceptor getCacheControlInterceptor() {
        return chain -> {
            Response originalResponse = chain.proceed(chain.request());
            return originalResponse.newBuilder()
                    .removeHeader("Cache-Control")
                    .header("Cache-Control", "public, max-age=60")
                    .build();
        };
    }
}
