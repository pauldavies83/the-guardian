package uk.co.pauldavies83.theguardian.articles

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.list_item_article.view.*
import kotlinx.android.synthetic.main.list_item_divider.view.*
import uk.co.pauldavies83.theguardian.R
import uk.co.pauldavies83.theguardian.articles.model.*
import java.text.SimpleDateFormat
import java.util.*

class ArticleAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var articles = emptyList<ArticleListEntry>()
    val recyclerViewItemSelectedObservable = PublishSubject.create<Article>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            DIVIDER_VIEW_TYPE -> {
                val view = layoutInflater.inflate(R.layout.list_item_divider, parent, false)
                ArticleDividerViewHolder(view)
            }
            else -> {
                val view = layoutInflater.inflate(R.layout.list_item_article, parent, false)
                ArticleItemViewHolder(view)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            articles[position] is ArticleListEntryDivider -> DIVIDER_VIEW_TYPE
            else -> ENTRY_VIEW_TYPE
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when {
            articles[position] is ArticleListEntryDivider -> {
                val articleDividerViewHolder = holder as ArticleDividerViewHolder
                articleDividerViewHolder.bind(articles[position] as ArticleListEntryDivider)
            }
            articles[position] is ArticleListEntryItem -> {
                val articleItemViewHolder = holder as ArticleItemViewHolder
                articleItemViewHolder.bind(articles[position] as ArticleListEntryItem)
            }
        }
    }

    override fun getItemCount(): Int {
        return articles.size
    }

    fun showArticles(articles: List<ArticleListEntry>) {
        this.articles =  articles
        notifyDataSetChanged()
    }

    internal inner class ArticleItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val headlineTextView = view.article_headline_textview
        val publishedDateTextView = view.article_published_date_textview
        val thumbnailImageView = view.article_thumbnail_imageview

        fun bind(articleListEntry: ArticleListEntryItem) {
            itemView.setOnClickListener { ignored -> recyclerViewItemSelectedObservable.onNext(articleListEntry.article) }
            itemView.contentDescription = articleListEntry.article.title
            headlineTextView.text = articleListEntry.article.title
            publishedDateTextView.text = formatDateString(articleListEntry.article.published)
            Glide.with(itemView.context)
                    .load(articleListEntry.article.thumbnail)
                    .apply(RequestOptions.circleCropTransform())
                    .into(thumbnailImageView!!)
        }

        private fun formatDateString(date: Date): String {
            val simpleDateFormat = SimpleDateFormat(itemView.context.getString(R.string.date_format_template), Locale.UK)
            return simpleDateFormat.format(date)
        }
    }

    internal inner class ArticleDividerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val articleDividerLabel = itemView.article_divider_label

        fun bind(divider: ArticleListEntryDivider) {
            when (divider) {
                is FavouritesDivider -> articleDividerLabel.text = itemView.context.getString(R.string.divider_label_favourites)
                is ThisWeekDivider -> articleDividerLabel.text = itemView.context.getString(R.string.divider_label_this_week)
                is LastWeekDivider -> articleDividerLabel.text = itemView.context.getString(R.string.divider_label_last_week)
                is OlderDivider -> articleDividerLabel.text = itemView.context.getString(R.string.divider_label_older)
            }
        }
    }

    companion object {
        private val DIVIDER_VIEW_TYPE = 1
        private val ENTRY_VIEW_TYPE = 2
    }
}
