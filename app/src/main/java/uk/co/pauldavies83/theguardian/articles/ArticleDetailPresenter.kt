package uk.co.pauldavies83.theguardian.articles

import io.reactivex.Observable
import io.reactivex.Scheduler
import uk.co.pauldavies83.theguardian.articles.model.Article
import uk.co.pauldavies83.theguardian.articles.model.ArticleDetail
import uk.co.pauldavies83.theguardian.common.BasePresenter
import uk.co.pauldavies83.theguardian.common.BasePresenterView

class ArticleDetailPresenter(val uiScheduler: Scheduler, val ioScheduler: Scheduler,
                             private val articlesRepository: ArticlesRepository, private val article: Article)
    : BasePresenter<ArticleDetailPresenter.View>() {

    override fun register(view: View) {
        super.register(view)

        addToUnsubscribe(articlesRepository.getArticle(article.url)
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)
            .subscribe { result ->
                run {
                    when {
                        result is SuccessResponse<ArticleDetail> -> view.showArticleDetail(result.data)
                        else -> view.showError()
                    }
                }
            })

        addToUnsubscribe(articlesRepository.isArticleFavourite(article)
            .subscribeOn(ioScheduler)
            .observeOn(uiScheduler)
            .subscribe { result ->
                run {
                    when {
                        result is SuccessResponse<Boolean> -> view.setArticleIsFavourite(result.data)
                        else -> view.setArticleIsFavourite(false)
                    }
                }
            })

        addToUnsubscribe(view.onArticleFavouriteToggled()
            .switchMapSingle { articlesRepository.toggleArticleFavourited(article).subscribeOn(ioScheduler).observeOn(uiScheduler) }
            .subscribe { result ->
                run {
                    when {
                        result is SuccessResponse<Boolean> -> view.setArticleIsFavourite(result.data)
                        else -> view.setArticleIsFavourite(false)
                    }
                }
            })

    }

    interface View : BasePresenterView {
        fun showArticleDetail(articleDetail: ArticleDetail)
        fun showError()

        fun onArticleFavouriteToggled() : Observable<Article>
        fun setArticleIsFavourite(isFavourite: Boolean)
    }

}