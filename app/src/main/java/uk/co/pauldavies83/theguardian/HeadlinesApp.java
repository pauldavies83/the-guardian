package uk.co.pauldavies83.theguardian;

import android.app.Application;
import android.content.Context;

import uk.co.pauldavies83.theguardian.articles.ArticlesModule;


public class HeadlinesApp extends Application {
    private final ArticlesModule articlesModule = new ArticlesModule();

    public static ArticlesModule from(Context applicationContext) {
        return ((HeadlinesApp) applicationContext).articlesModule;
    }
}
