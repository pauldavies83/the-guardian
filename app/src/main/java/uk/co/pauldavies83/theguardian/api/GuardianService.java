package uk.co.pauldavies83.theguardian.api;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;
import uk.co.pauldavies83.theguardian.api.model.ApiArticleFieldsRoot;
import uk.co.pauldavies83.theguardian.api.model.ApiArticleListRoot;

public interface GuardianService {
    @GET("search?show-fields=headline,thumbnail&page-size=200&order-by=newest")
    Single<Response<ApiArticleListRoot>> searchArticles(@Query("q") String searchTerm);

    @GET
    Single<Response<ApiArticleFieldsRoot>> getArticle(@Url String articleUrl, @Query("show-fields") String fields);
}
